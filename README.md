sudo apt-get update   
sudo apt-get upgrade   


sudo apt-get install git mysql-server   
sudo adduser --disabled-login --gecos 'Gitea' git   
sudo mysql_secure_installation    (no root password, press enter)   


# Setting up the mysql server
mysql -u root -p   
CREATE DATABASE gitea;   
GRANT ALL PRIVILEGES ON gitea.* TO 'gitea'@'localhost' IDENTIFIED BY 'ENTERPASSWORD';   
FLUSH PRIVILEGES;   
exit   



sudo su git   
cd ~   
mkdir gitea   
cd gitea   

# Download Gitea
wget https://dl.gitea.io/gitea/1.5/gitea-1.5-linux-arm-7   
chmod +x gitea   



# Setting up a Gitea service
sudo vim /etc/systemd/system/gitea.service   

[Unit]   
Description=Gitea (Git with a cup of tea)   
After=syslog.target   
After=network.target   

[Service]   
\# Modify these two values ​​and uncomment them if you have   
\# repos with lots of files and get to HTTP error 500 because of that   
###   
\# LimitMEMLOCK=infinity   
\# LimitNOFILE=65535   
RestartSec=2s   
Type=simple   
User=git   
Group=git   
WorkingDirectory=/home/git/gitea   
ExecStart=/home/git/gitea/gitea web   
Restart=always   
Environment=USER=git    
HOME=/home/git   

[Install]   
WantedBy=multi-user.target   



sudo systemctl enable gitea.service   
sudo systemctl start gitea.service   